import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { HomeComponent } from './home/home.component';
import { DemoComponent } from './demo/demo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExamplePipe } from './shared/pipes/example.pipe';
import { CardComponent } from './card/card.component';
import { ConnectComponent } from './connect/connect.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    DemoComponent,
    ExamplePipe,
    CardComponent,
    ConnectComponent,
    EditContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, //Pour pouvoir utiliser la directive ngModel
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
