import { Component, OnInit } from '@angular/core';
import { IUser } from '../models/IUser';
import { FakeauthService } from '../shared/fakeauth.service';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent implements OnInit {
  connectedUser : IUser | undefined;

  login : string = '';
  pwd : string = '';

  constructor(private _authService : FakeauthService){

  }

  ngOnInit() : void {
    this._authService.$connectedUser.subscribe({
      next : (response : IUser | undefined) => {
        console.log("Connect Component Coucou");
        
        this.connectedUser = response;
      },
      error : () => {},
      complete : () => {}
    })

    //this._authService.$connectedUser.subscribe(() => {})
  }

  connect() : void {
    //this.connectedUser = this._authService.login(this.login, this.pwd);
    this._authService.login(this.login, this.pwd);
  }

  disconnect() : void {
    //this.connectedUser = this._authService.logout();
    this._authService.logout();
  }

}
