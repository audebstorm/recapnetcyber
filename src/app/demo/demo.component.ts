import { Component, OnInit } from '@angular/core';
import { IFormateur } from '../models/IFormateur';
import { IUser } from '../models/IUser';
import { FakeauthService } from '../shared/fakeauth.service';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  //Bindings
  prenom : string = "Aude";
  nom : string = "";
  monPlaceholder : string = "Veuillez entrer un nom";
  buttonDisabled : boolean = false;

  //Directives
  monBg : string = "green";
  isGros : boolean = false;

  jour : boolean = true;

  boisson : string = '';

  formateurs : IFormateur[] = [
    { id : 1, lastname : 'Beurive', firstname : 'Aude', avatar : '../../assets/images/aude.png', available : true},
    { id : 2, lastname : 'Ly', firstname : 'Khun', avatar : '../../assets/images/khun.png', available : false},
    { id : 3, lastname : 'Chaineux', firstname : 'Gavin', avatar : '../../assets/images/gavin.png', available : true}
  ];

  formateurDuJour : string = '... [en attente de selection] ...';

  //Observables
  connectedUser : IUser | undefined;

  constructor(private _authService : FakeauthService) {
    
  }

  ngOnInit(): void {
    this._authService.$connectedUser.subscribe({
      next : (response : IUser | undefined) => {
        console.log("Demo Component Coucou");
        console.log(response);
        
        this.connectedUser = response;
      },
      error : () => {},
      complete : () => {}
    })
  }

  //Méthodes

  //Event Binding
  toggleButtonDisab() : void {
    this.buttonDisabled = !this.buttonDisabled;
  }

  //Class Directive
  toggleGros() : void {
    this.isGros = !this.isGros;
  }

  //Directive If
  toggleJour() : void {
    this.jour = !this.jour;
  }

  //Output
  changeFormateur(formateurName : string) : void {
    this.formateurDuJour = formateurName;
  }

}
