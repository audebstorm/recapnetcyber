export interface IFormateur {
    id : number;
    lastname : string;
    firstname : string;
    avatar : string;
    available : boolean;
}