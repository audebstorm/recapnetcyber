import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IFormateur } from '../models/IFormateur';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  // formateurAAfficher : IFormateur = {
  //   id : 4,
  //   lastname : "Oui",
  //   firstname : "Non",
  //   avatar : '',
  //   available : true
  // }
  @Input() formateurAAfficher! : IFormateur;
  //-> autant que vous voulez tant que vous respectez les types
  //Input = recevoir

  @Output() onSelect : EventEmitter<string>;
  //Ouput = renvoyer quelque chose

  constructor(){
    this.onSelect = new EventEmitter<string>(); //toujours initialiser notre EventEmitter
  }

  sendName() {
    this.onSelect.emit(this.formateurAAfficher.firstname);
    //ou
    //this.onSelect.next(this.formateurAAfficher.firstname);
  }
}
