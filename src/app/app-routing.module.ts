import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DemoComponent } from './demo/demo.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path : '', component : HomeComponent},
  { path : 'home', redirectTo : ''},
  { path : 'demo', component : DemoComponent},
  //{ path : 'contacts', component : ContactsComponent},
  { path : 'editContact/:id', component : EditContactComponent }
  //{ path : '**', component : NotFoundComponent } //A mettre à la fin
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
