import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'example'
})
export class ExamplePipe implements PipeTransform {

  //temperature | example : 'f'
  transform(value: number, temp : string): number {
    switch(temp) {
      case 'f' :
        return value * (9/5) + 32;
      case 'c' : 
        return (value - 32) * 5/9;
      default :
        return value;

    }
  }

}
